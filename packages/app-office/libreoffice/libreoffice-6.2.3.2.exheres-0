# Copyright 2008-2011, 2014 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2011 Dimitry Ishenko <dimitry.ishenko@gmail.com>
# Copyright 2011-2013 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# list is in solenv/inc/langlist.mk
linguas=(
    af am ar as ast be bg bn bn_IN bo br brx bs ca ca_valencia cs cy da de dgo dz el en_GB en_US
    en_ZA eo es et eu fa fi fr ga gd gl gu gug he hi hr hu id is it ja ka kk km kmr_Latn kn ko kok
    ks lb lo lt lv mai mk ml mn mni mr my nb ne nl nn nr nso oc om or pa_IN pl pt pt_BR ro ru rw
    sa_IN sat sd sr_Latn si sid sk sl sq sr ss st sv sw_TZ ta te tg th tn tr ts tt ug uk uz ve vi xh
    zh_CN zh_TW zu
)

# this is down here because it uses `linguas`
require libreoffice-6 autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]

# When bumping, check download.lst for new tarballs
DOWNLOADS+="
    (
        ${LIBRE_DEV_URL}26b3e95ddf3d9c077c480ea45874b3b8-lp_solve_5.5.tar.gz
        ${LIBRE_DEV_URL}a8c2c5b8f09e7ede322d5c602ff6a4b6-mythes-1.2.4.tar.gz
        ${LIBRE_DEV_URL}0168229624cfac409e766913506961a8-ucpp-1.3.2.tar.gz
    ) [[ note = [ bundled libraries/tools
        lp-solve - seems dead upstream, if you want to package it, look at bugs.fdo#39496
    ] ]]

    (
        ${LIBRE_DEV_URL}1f467e5bb703f12cbbb09d5cf67ecf4a-converttexttonumber-1-5-0.oxt
        ${LIBRE_DEV_URL}b7cae45ad2c23551fd6ccb8ae2c1f59e-numbertext_0.9.5.oxt
    ) [[ note = [ extensions
        unpackaged because they need java:
        Google Docs, Diagram/SmART, LanguageTool, NLPSolver, Validator, Watch Window
    ] ]]

    (
        ${LIBRE_DEV_URL}twaindsm_2.4.1.orig.tar.gz
    ) [[ note = [ unpackaged and configure.ac doesn't mention it, so it appears
        that it only uses its headers
    ] ]]

    (
        java? ( ${LIBRE_DEV_URL}17410483b5b5f267aa18b7e00b65e6e0-hsqldb_1_8_0.zip )
    ) [[ note = [ It only works with hsqldb>=1.8.0.9&<1.8.1 As those versions are rather old I don't
                  necessarily see a point in unbundling them. ] ]]
"

PLATFORMS="~amd64 ~x86"

# osl_StreamPipe fails
RESTRICT="test"

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/${PN}-5.3.4.2-kioclient5.patch
)

src_prepare() {
    libreoffice-6_src_prepare

    eautoreconf
}

