# Copyright 2013 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=tdf release=v${PV} suffix=tar.gz ] alternatives

export_exlib_phases src_install

SUMMARY="C++ client library for the CMIS interface"
DESCRIPTION="
Allows C++ applications to connect to any ECM behaving as a CMIS server like Alfresco or Nuxeo
"

LICENCES="|| ( MPL-1.1 GPL-2 GPL-3 LGPL-2 LGPL-3 )"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/boost[>=1.36.0]
        dev-libs/libxml2:2.0
        net-misc/curl
    test:
        dev-cpp/cppunit[>=1.12]
"

# man pages need docbook2X (unwritten)
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-client
    --disable-static
    --disable-werror
    --without-man
)
DEFAULT_SRC_CONFIGURE_TESTS=( '--enable-tests --disable-tests' )

libcmis_src_install() {
    default

    alternatives_for _$(exhost --target)_${PN} ${PN}-${SLOT} ${SLOT#0.}   \
        /usr/share/man/manx/cmis-client{,-${SLOT}}.xml \
        /usr/$(exhost --target)/bin/cmis-client{,-${SLOT}}
}

